from base_script import BaseScript

class Script(BaseScript):
    _conf_vars = ['aws_access_key_id', 'aws_secret_access_key']
